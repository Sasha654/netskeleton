﻿using Serilog;
using System;

namespace Shared.Utils.Extensions
{
    public static class SerilogExt
    {
        /// <summary>
        /// Configures the sink if used
        /// </summary>
        /// <param name="loggerConfiguration"></param>
        /// <param name="useSink"></param>
        /// <param name="configBuilder"></param>
        /// <returns></returns>
        public static LoggerConfiguration IfUseSink(
            this LoggerConfiguration loggerConfiguration,
            bool useSink,
            Func<LoggerConfiguration, LoggerConfiguration> configBuilder)
        {
            if(useSink)
            {
                return configBuilder(loggerConfiguration);
            }

            return loggerConfiguration;
        }
    }
}
