﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Web.BaseApi.SignalRHubs
{
    public class TestHub : Hub
    {
        private string _receiveTimeClientMethod = "receiveTime";
        private ConcurrentDictionary<string, IClientProxy> _clients;
        private CompositeDisposable _disposable;

        public TestHub()
        {
            _clients = new ConcurrentDictionary<string, IClientProxy>();
            _disposable = new CompositeDisposable();
            StartPushTimeToActivatedClients();
        }

        private void StartPushTimeToActivatedClients()
        {
            var stream = Observable
                .Interval(TimeSpan.FromSeconds(2))
                .Select(_ => DateTime.UtcNow.ToLongTimeString());

            var subscription = stream
                .Subscribe(
                    async time =>
                    {
                        foreach (var item in _clients)
                            await item.Value.SendAsync(_receiveTimeClientMethod, time);
                    });

            _disposable.Add(subscription);
        }

        public void Activate()
        {
            _clients.GetOrAdd(Context.ConnectionId, Clients.Caller);
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            _clients.TryRemove(Context.ConnectionId, out _);
            return base.OnDisconnectedAsync(exception);
        }

        protected override void Dispose(bool disposing)
        {
            _disposable.Dispose();
            base.Dispose(disposing);
        }
    }
}
