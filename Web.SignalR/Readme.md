### Used stack:
1. ASP.NET Core Web API (.NET Core 3.1) and [SignalR](https://dotnet.microsoft.com/apps/aspnet/signalr)
2. MSTest
3. [Autofac](https://autofac.org/)
4. [Serilog](https://github.com/serilog/serilog-aspnetcore/) for ASP.NET Core
5. [Newtonsoft.Json](https://www.newtonsoft.com/json)
6. [Rx](http://reactivex.io/)

This solution shows how server-side code sends asynchronous notifications to client-side web applications using SignalR. SignalR will use WebSockets.
