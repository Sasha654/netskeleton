﻿using DevExpress.Mvvm;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace DxBase.ViewModels
{
    public class TimeViewModel : ViewModelBase
    {
        private SerialDisposable _timeDisposable;
        private IObservable<DateTime> _currentTimeStream;

        public TimeViewModel()
        {
            CurrentTime = default(DateTime).ToLongTimeString();
            LoadCommand = new DelegateCommand(LoadCommandExecute);
            EnableTimeCommand = new AsyncCommand(EnableTimeCommandExecute, () => _timeDisposable?.IsDisposed ?? true);
            DisableTimeCommand = new AsyncCommand(DisableTimeCommandExecute, () => !_timeDisposable?.IsDisposed ?? false);
        }

        public string CurrentTime
        {
            get => GetValue<string>(nameof(CurrentTime));
            set => SetValue(value, nameof(CurrentTime));
        }

        public DelegateCommand LoadCommand { get; set; }
        public AsyncCommand EnableTimeCommand { get; set; }
        public AsyncCommand DisableTimeCommand { get; set; }

        private void LoadCommandExecute()
        {
            _currentTimeStream = Observable
                .Timer(TimeSpan.Zero, TimeSpan.FromSeconds(1))
                .Select(_ => DateTime.Now);
        }

        private Task EnableTimeCommandExecute()
        {
            _timeDisposable = new SerialDisposable();
            _timeDisposable.Disposable = _currentTimeStream
                .Subscribe(
                    onNext: time => CurrentTime = time.ToLongTimeString());
            return Task.CompletedTask;
        }

        private Task DisableTimeCommandExecute()
        {
            _timeDisposable.Dispose();
            CurrentTime = default(DateTime).ToLongTimeString();
            return Task.CompletedTask;
        }
    }
}
