﻿using System;

namespace Network.NetMq.NetConnection
{
    public interface INetClient
    {
        IObservable<T> ConsumeStream<T>() where T : class;
    }
}
