﻿namespace Network.NetMq.NetConnection
{
    public interface INetConnectionFactory
    {
        INetServer CreateServer();

        INetClient CreateClient();
    }
}
