﻿using System;

namespace Network.NetMq.NetConnection
{
    public interface INetServer
    {
        IDisposable PublishStream<T>(IObservable<T> stream) where T : class;
    }
}
