﻿using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;
using System;
using Network.NetMq.NetConnection;

namespace Network.NetMq.NetMqConnection
{
    public class NetMqPublisher : INetServer
    {
        private readonly XPublisherSocket _socket;

        public NetMqPublisher(XPublisherSocket socket)
        {
            _socket = socket;
        }

        public IDisposable PublishStream<T>(IObservable<T> stream) where T : class
        {
            var topic = typeof(T).Name;
            var subscription = stream.Subscribe(
                m =>
                {
                    var json = JsonConvert.SerializeObject(m);
                    _socket
                        .SendMoreFrame(topic)
                        .SendFrame(json);
                });

            return subscription;
        }
    }
}
