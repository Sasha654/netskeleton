﻿using NetMQ.Sockets;
using Network.NetMq.NetConnection;
using Network.NetMq.NetMqConnection;

namespace Network.NetMq.NetMqConection
{
    public class NetMqPubSubFactory : INetConnectionFactory
    {
        private int _port;

        public NetMqPubSubFactory(int port  = 5054)
        {
            _port = port;
        }

        public INetServer CreateServer()
        {
            var outgoingSocket = new XPublisherSocket();
            outgoingSocket.Bind($"tcp://*:{_port}");
            return new NetMqPublisher(outgoingSocket);
        }

        public INetClient CreateClient()
        {
            var ingoingSocket = new SubscriberSocket();
            ingoingSocket.Connect($"tcp://localhost:{_port}");
            return new NetMqSubscriber(ingoingSocket);
        }
    }
}
