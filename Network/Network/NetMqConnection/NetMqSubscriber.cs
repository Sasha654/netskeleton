﻿using NetMQ;
using NetMQ.Sockets;
using Network.NetMq.NetConnection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace Network.NetMq.NetMqConection
{
    public class NetMqSubscriber : INetClient
    {
        private readonly SubscriberSocket _socket;
        private readonly int _expectedFrameCountByMessage = 2;
        private readonly TimeSpan _frameTimeout = TimeSpan.FromSeconds(5);

        public NetMqSubscriber(SubscriberSocket socket)
        {
            _socket = socket;
        }

        public IObservable<T> ConsumeStream<T>() where T : class
        {
            return Observable.Create<T>(observer =>
            {
                var topic = typeof(T).Name;
                var tryReceive = true;
                _socket.Subscribe(topic);

                Task.Factory.StartNew(() =>
                {
                    try
                    {
                        var frames = new List<string>(_expectedFrameCountByMessage);
                        while (tryReceive)
                        {
                            var isReceived = _socket.TryReceiveMultipartStrings(
                                _frameTimeout,
                                ref frames,
                                _expectedFrameCountByMessage);

                            if (isReceived && frames.FirstOrDefault() == topic)
                            {
                                var msg = JsonConvert.DeserializeObject<T>(frames.Last());
                                observer.OnNext(msg);
                            }
                        }

                        observer.OnCompleted();
                    }
                    catch (Exception e)
                    {
                        observer.OnError(e);
                    }
                });

                return Disposable.Create(() => tryReceive = false);
            });
        }
    }
}
