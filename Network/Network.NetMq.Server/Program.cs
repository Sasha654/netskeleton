﻿using Network.NetMq.NetMqConection;
using System;
using System.Reactive.Linq;
using Network.Messages;

namespace Network.NetMq.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "SERVER";

            var stream = Observable
                .Interval(TimeSpan.FromSeconds(1))
                .Select(i => new TestMessage($"Ping N{i}"))
                .Do(m => Console.WriteLine($"Sent: <{m.Text}>"));

            var factory = new NetMqPubSubFactory();
            var server = factory.CreateServer();
            var res = server.PublishStream(stream);

            Console.ReadLine();
            res.Dispose();
        }
    }
}
