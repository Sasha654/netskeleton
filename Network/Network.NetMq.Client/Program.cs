﻿using System;
using Network.Messages;
using Network.NetMq.NetMqConection;

namespace Network.NetMq.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "CLIENT";

            var factory = new NetMqPubSubFactory();
            var client = factory.CreateClient();
            var stream = client.ConsumeStream<TestMessage>();

            var subscription = stream.Subscribe(
                m => Console.WriteLine($"Received: <{m.Text}>"),
                e => Console.WriteLine(e.Message),
                () => Console.WriteLine("Completed"));

            Console.ReadLine();
            subscription.Dispose();
        }
    }
}
