### Used stack:
1. ASP.NET Core Web API (.NET Core 3.1)
2. MSTest
3. [Autofac](https://autofac.org/)
4. [Serilog](https://github.com/serilog/serilog-aspnetcore/) for ASP.NET Core
5. [Newtonsoft.Json](https://www.newtonsoft.com/json)
