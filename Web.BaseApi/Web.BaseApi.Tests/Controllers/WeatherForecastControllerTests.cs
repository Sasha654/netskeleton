﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Serilog;
using Web.BaseApi.Services;

namespace Web.BaseApi.Controllers.Tests
{
    [TestClass]
    public class WeatherForecastControllerTests
    {
        private WeatherForecastController _controller;

        public WeatherForecastControllerTests()
        {
            var weatherForecast = new RandomWeather();
            _controller = new WeatherForecastController(weatherForecast, Log.Logger);
        }

        [TestMethod]
        public void GetTest()
        {
            var forecast = _controller.GetAsync("moscow", 7).GetAwaiter().GetResult().Value;

            Assert.IsInstanceOfType(forecast, typeof(WeatherForecastDto));
            Assert.AreEqual(7, forecast.Forecasts.Count);
            Assert.AreEqual("Moscow", forecast.City);
        }
    }
}
