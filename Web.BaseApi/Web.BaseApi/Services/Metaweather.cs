﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Web.BaseApi.Services
{
    /// <summary>
    /// Weather forecasts from https://www.metaweather.com
    /// </summary>
    public class Metaweather : IWeatherForecast
    {
        private const string SearchWoeidUrl = @"https://www.metaweather.com/api/location/search/?query={0}";
        private const string ForecastUrl = @"https://www.metaweather.com/api/location/{0} ";

        public async Task<WeatherForecastDto> GetForecast(string city, int days)
        {
            var woeid = await GetWoeid(city);
            var forecasts = await GetWeatherForecasts(woeid, days);
            return new WeatherForecastDto(city, forecasts);
        }

        private async Task<string> GetWoeid(string city)
        {
            var url = string.Format(SearchWoeidUrl, city);
            var jsonLocation = await GetJsonResponse(url);
            dynamic locationArray = JArray.Parse(jsonLocation);
            dynamic location = locationArray[0];
            var woeid = location.woeid;
            return woeid;
        }

        private async Task<List<Forecast>> GetWeatherForecasts(string woeid, int days)
        {
            var url = string.Format(ForecastUrl, woeid);
            var jsonForecasts = await GetJsonResponse(url);
            dynamic forecastsObject = JsonConvert.DeserializeObject(jsonForecasts);
            dynamic forecastsArray = forecastsObject.consolidated_weather;
            var forecasts = new List<Forecast>();
            
            foreach (var item in forecastsArray)
            {
                int temperatureC = item.the_temp;
                string dtString = item.applicable_date;
                var dt = DateTime.Parse(dtString);
                forecasts.Add(new Forecast(dt, temperatureC));
                if (forecasts.Count == days) break;
            }

            return forecasts;
        }

        private async Task<string> GetJsonResponse(string url)
        {
            var request = WebRequest.Create(url);

            using (var response = await request.GetResponseAsync())
            using (var stream = response.GetResponseStream())
            using (var sr = new StreamReader(stream))
            {
                var json = await sr.ReadToEndAsync();
                return json;
            }
        }
    }
}
