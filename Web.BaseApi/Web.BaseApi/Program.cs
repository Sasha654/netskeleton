using System;
using System.IO;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using Shared.Utils.Extensions;

namespace Web.BaseApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false)
                .Build();

            InitializeLogging(config);

            try
            {
                Log.Information($"App <{nameof(Web.BaseApi)}> starting...");
                CreateAndRunHost(config, args);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, $"App <{nameof(Web.BaseApi)}> crashed");
            }
            finally
            {
                Log.Information($"App <{nameof(Web.BaseApi)}> stopped");
                Log.CloseAndFlush();
            }
        }

        private static void CreateAndRunHost(IConfigurationRoot config, string[] args)
        {
            var port = config.GetValue<int>("Kestrel:Port");

            var host = Host
                .CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                        .UseKestrel(options => options.ListenAnyIP(port))
                        .UseStartup<Startup>()
                        .UseSerilog();
                })
                .Build();

            host.Run();
        }

        private static void InitializeLogging(IConfigurationRoot config)
        {
            var commonLevel = config.GetValue<LogEventLevel>("Serilog:CommonLevel");
            var useFileSink = config.GetValue<bool>("Serilog:UseFileSink");

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Is(commonLevel)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .IfUseSink(useFileSink, loggerConfig =>
                {
                    return loggerConfig.WriteTo.File(
                        path: Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $@"logs\{nameof(Web.BaseApi)}_.log"),
                        rollingInterval: RollingInterval.Day,
                        buffered: true,
                        flushToDiskInterval: TimeSpan.FromSeconds(5));
                })
                .CreateLogger();
        }
    }
}
