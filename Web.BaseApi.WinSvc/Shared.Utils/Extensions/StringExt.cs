﻿namespace Shared.Utils.Extensions
{
    public static class StringExt
    {
        public static string Capitalize(this string str)
        {
            if (string.IsNullOrEmpty(str) || char.IsUpper(str[0]))
            {
                return str;
            }

            return char.ToUpper(str[0]) + str.Substring(1);
        }
    }
}
