### Used stack:
1. ASP.NET Core Web API (.NET Core 3.1)
2. MSTest
3. [Autofac](https://autofac.org/)
4. [Serilog](https://github.com/serilog/serilog-aspnetcore/) for ASP.NET Core
5. [Topshelf](http://topshelf-project.com/)

Topshelf makes it possible to debug Windows services as console applications.<br/>
This solution shows how to start the web server as a Windows service using Topshelf.<br/>
First you must specify runtime of the ASP.NET Core app in the project file:
```
<RuntimeIdentifier>win10-x64</RuntimeIdentifier>
```
Set the service name in `appsettings.json`, e.g:
```
"WindowsSvcName": "_Web.BaseApi"
```
After publish the project:
```
dotnet publish Web.BaseApi/Web.BaseApi.csproj -c release -r win10-x64
```
Install:
```
.\Web.BaseApi.exe install
``` 
Run as a Windows service:
```
net start _Web.BaseApi
```
