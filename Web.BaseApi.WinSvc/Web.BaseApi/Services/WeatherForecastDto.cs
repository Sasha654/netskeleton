using System;
using System.Collections.Generic;
using System.Linq;

namespace Web.BaseApi.Services
{
    public class WeatherForecastDto
    {
        public WeatherForecastDto(string city, IEnumerable<Forecast> forecasts)
        {
            City = city;
            Forecasts = forecasts.ToList().AsReadOnly();
        }

        public string City { get; }

        public IReadOnlyList<Forecast> Forecasts { get; }
    }


    public class Forecast
    {
        public Forecast(DateTime date, int temperatureC)
        {
            Date = date;
            TemperatureC = temperatureC;
        }

        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
    }
}
