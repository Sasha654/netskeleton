﻿using System.Threading.Tasks;

namespace Web.BaseApi.Services
{
    public interface IWeatherForecast
    {
        Task<WeatherForecastDto> GetForecast(string city, int days);
    }
}
