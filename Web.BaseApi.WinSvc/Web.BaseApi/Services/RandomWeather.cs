﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Shared.Utils.Extensions;

namespace Web.BaseApi.Services
{
    public class RandomWeather : IWeatherForecast
    {
        private int _minTemperatureC = -30;
        private int _maxTemperatureC = 40;
        private Random _randomGenerator;

        public RandomWeather()
        {
            _randomGenerator = new Random();
        }

        public Task<WeatherForecastDto> GetForecast(string city, int days)
        {
            return Task.Run(() =>
            {
                var capitalizeCity = city.Capitalize();

                var forecasts = Enumerable
                    .Range(1, days)
                    .Select(dayIndex =>
                    {
                        var dt = DateTime.UtcNow.AddDays(dayIndex);
                        return new Forecast(
                            new DateTime(dt.Year, dt.Month, dt.Day),
                            _randomGenerator.Next(_minTemperatureC, _maxTemperatureC));
                    });

                return new WeatherForecastDto(capitalizeCity, forecasts);
            });
        }
    }
}
