using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using Shared.Utils.Extensions;
using Topshelf;

namespace Web.BaseApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false)
                .Build();

            var winSvcName = config.GetValue<string>("WindowsSvcName");
            var port = config.GetValue<int>("Kestrel:Port");

            InitializeLogging(config);

            try
            {
                Log.Information($"App <{nameof(Web.BaseApi)}> starting...");

                HostFactory.Run(topshelfHostConfig =>
                {
                    topshelfHostConfig.UseSerilog();
                    topshelfHostConfig.SetServiceName(winSvcName);
                    topshelfHostConfig.RunAsLocalService();

                    topshelfHostConfig.Service<KestrelAsWindowsSvc>(svcConfig =>
                    {
                        svcConfig.ConstructUsing(() => new KestrelAsWindowsSvc());
                        svcConfig.WhenStarted(s => s.Start(port, args));
                        svcConfig.WhenStopped(s => s.Stop());
                    });

                    topshelfHostConfig.StartAutomaticallyDelayed();
                });
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, $"App <{nameof(Web.BaseApi)}> crashed");
            }
            finally
            {
                Log.Information($"App <{nameof(Web.BaseApi)}> stopped");
                Log.CloseAndFlush();
            }
        }

        private static void InitializeLogging(IConfigurationRoot config)
        {
            var commonLevel = config.GetValue<LogEventLevel>("Serilog:CommonLevel");
            var useFileSink = config.GetValue<bool>("Serilog:UseFileSink");

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Is(commonLevel)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .IfUseSink(useFileSink, loggerConfig =>
                {
                    return loggerConfig.WriteTo.File(
                        path: Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $@"logs\{nameof(Web.BaseApi)}_.log"),
                        rollingInterval: RollingInterval.Day,
                        buffered: true,
                        flushToDiskInterval: TimeSpan.FromSeconds(5));
                })
                .CreateLogger();
        }
    }
}
