﻿using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace Web.BaseApi
{
    public class KestrelAsWindowsSvc
    {
        private IHost _webHost;

        public void Start(int port, string[] args)
        {
            _webHost = Host
                .CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                        .UseKestrel(options => options.ListenAnyIP(port))
                        .UseStartup<Startup>()
                        .UseSerilog();
                })
                .Build();

            _webHost.StartAsync();
        }

        public void Stop()
        {
            _webHost.StopAsync().GetAwaiter().GetResult();
        }
    }
}
