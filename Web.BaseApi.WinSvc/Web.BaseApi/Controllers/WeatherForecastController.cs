﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Web.BaseApi.Services;

namespace Web.BaseApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private ILogger _logger;
        private IWeatherForecast _weatherForecast;
        private int _defaultForecastDays = 7;
        private int _maxForecastDays = 32;

        public WeatherForecastController(IWeatherForecast weatherForecast, ILogger logger)
        {
            _logger = logger;
            _weatherForecast = weatherForecast;
        }

        //GET api/weatherforecast
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        //GET /api/weatherforecast/moscow/7
        [HttpGet("{city}/{days}")]
        public async Task<ActionResult<WeatherForecastDto>> GetAsync(string city, int days)
        {
            var numDays = days > 0 && days < _maxForecastDays ? days : _defaultForecastDays;
            var forecast = await _weatherForecast.GetForecast(city, numDays);
            return forecast;
        }

        //POST api/weatherforecast
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        //PUT api/weatherforecast/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        //DELETE api/weatherforecast/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
